//const accountOpenUrl = process.env.REACT_APP_OPEN_ACCOUNT_API_URL;
const OpenTicketUrl = "http://localhost:9111/api/ticket/customers";
const ticketsUrl = "http://localhost:9111/api/ticket/";
const perticularticketUrl = "http://localhost:9111/api/customers/ticket";

// const OpenTicketUrl = "http://35.238.21.162:9111/api/ticket/customers";
// const ticketsUrl = "http://35.238.21.162:9111/api/ticket/";
// const perticularticketUrl = "http://35.238.21.162:9111/api/customers/ticket";

const customerconcernAPIClient = {
    riaseConcern: function (p) {
        console.log(p);
        const request = new Request(OpenTicketUrl, {
            method: 'POST',
            headers: new Headers({
                'charset' : 'UTF-8',
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(p) 
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    getTicketDetails: function (p) {

        var promise = new Promise((resolve, reject) => {
            return fetch(perticularticketUrl+p).then((res) => {
                var result = res.json();
                result.then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("Invalid Account Id entered.");
                });
            }).catch((err) => {
                    reject("Error connecting to the API");
            });
        });
    
        return promise;
    }

}
export default customerconcernAPIClient;


