// const accountOpenUrl = "http://localhost:9222/api/account/openAccount";
// const balanceUrl = "http://localhost:9222/api/account/balance/";
// const accountUrl = "http://localhost:9222/api/account/";
// const transactionUrl = "http://localhost:9222/api/account/transaction/";

const accountOpenUrl = "http://34.69.94.192:9222/api/account/openAccount";
const balanceUrl = "http://34.69.94.192:9222/api/account/balance/";
const accountUrl = "http://34.69.94.192:9222/api/account/";
const transactionUrl = "http://34.69.94.192:9222/api/account/transaction/";
const getStatementByDatePostfix = "/statementByDate?start_date="
const endDatePostfix = "&end_date="

const statementUrlPostfix = "/statement"

const accountAPIClient = {
    getStatementByDate: function (accountId, startDate, endDate) {
        const statementUrl = accountUrl + accountId + getStatementByDatePostfix + startDate + endDatePostfix + endDate;
        var promise = new Promise((resolve, reject) => {
            return fetch(statementUrl).then((res) => {
                var result = res.json();
                result.then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("Invalid Account Id/Date entered.");
                });
            }).catch((err) => {
                    reject("Error connecting to the API, try again later");
            });
        });
    
        return promise;
    },

    insertAccount: function (account) {
        console.log("in insert method");
        const request = new Request(accountOpenUrl, {
            method: 'POST',
            headers: new Headers({
                'charset' : 'UTF-8',
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(account) 
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("Fill all field with valid information");
                })
            }).catch(error => {
                reject("Error connecting to the API, try again later");
            });
        });

        return promise;
    },

    transact: function (accountId, transaction) {
        console.log("in insert method");
        const request = new Request(transactionUrl + accountId, {
            method: 'POST',
            headers: new Headers({
                'charset' : 'UTF-8',
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(transaction) 
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("Enter valid accountId and amount");
                })
            }).catch(error => {
                reject("Error connecting to the API, try again later");
            });
        });

        return promise;
    },

    getStatement: function (accountId) {

        var promise = new Promise((resolve, reject) => {
            return fetch(accountUrl+accountId+statementUrlPostfix).then((res) => {
                var result = res.json();
                result.then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("Invalid Account Id entered.");
                });
            }).catch((err) => {
                    reject("Error connecting to the API, try again later");
            });
        });
    
        return promise;
    },

    getAccountBalance: function (p) {

        var promise = new Promise((resolve, reject) => {
            return fetch(balanceUrl+p).then((res) => {
                var result = res.json();
                result.then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("Invalid Account Id entered.");
                });
            }).catch((err) => {
                    reject("Error connecting to the API, try again later");
            });
        });
    
        return promise;
    },

    getAccount: function (p) {

        var promise = new Promise((resolve, reject) => {
            return fetch(accountUrl+p).then((res) => {
                var result = res.json();
                result.then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("Invalid Account Id entered.");
                });
            }).catch((err) => {
                    reject("Error connecting to the API, try again later");
            });
        });
    
        return promise;
    }

}



export default accountAPIClient;