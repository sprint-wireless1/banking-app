//const accountOpenUrl = process.env.REACT_APP_OPEN_ACCOUNT_API_URL;
const loanApplyUrl = "http://localhost:9333/api/applyloans/";
const balanceUrl = "http://localhost:9333/api/loanlist";
const accountUrl = "http://localhost:9333/api/applyloans/";

const loanAPIClient = {
    insertAccount: function (p) {
        console.log("in insert method");
        const request = new Request(loanApplyUrl, {
            method: 'POST',
            headers: new Headers({
                'charset' : 'UTF-8',
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(p) 
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    getAccountBalance: function (p) {

        var promise = new Promise((resolve, reject) => {
            return fetch(balanceUrl+p).then((res) => {
                var result = res.json();
                result.then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("Invalid Account Id entered.");
                });
            }).catch((err) => {
                    reject("Error connecting to the API");
            });
        });
    
        return promise;
    },

    getAccount: function (p) {

        var promise = new Promise((resolve, reject) => {
            return fetch(accountUrl+p).then((res) => {
                var result = res.json();
                result.then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("Invalid Account Id entered.");
                });
            }).catch((err) => {
                    reject("Error connecting to the API");
            });
        });
    
        return promise;
    }

}



export default loanAPIClient;



