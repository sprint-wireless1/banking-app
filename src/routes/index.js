import { lazy, Suspense } from "react";
import { Route, Switch, Link, Redirect } from "react-router-dom";

// Eager Loading
import HomeComponent from "../components/home/HomeComponent";
import LoaderAnimation from '../components/common/LoaderAnimation';
import NewAccountComponent from '../components/newAccount/NewAccountComponent'
import StatementComponent from "../components/statement/StatementComponent";
import NewLoanComponent from '../components/newLoan/NewLoanComponent';

import NewCustomerConcernConmponent from "../components/newconcern/NewCustomerConcernConmponent";
import CreateNewTicket from "../components/newconcern/CreateNewTicket";
import MyOpenTicketComponent from "../components/newconcern/MyOpenTicketComponent";

// Lazy Loading (On-Demnad Loading)
const AccountComponent = lazy(() => import("../components/account/AccountComponent"));
const TransactionComponent = lazy(() => import("../components/transaction/TransactionComponent"));
const CustomerConcernComponent = lazy(() => import("../components/CustomerService/CustomerConcernComponent"));

const img404 = require('../assets/http-404.jpg');

export default (
    <Suspense fallback={<LoaderAnimation />}>
        <Switch>
            <Route exact path="/" component={HomeComponent} />
            <Route path="/newAccount" component={NewAccountComponent} />
            <Route path="/account" component={AccountComponent} />
            <Route path="/transaction" component={TransactionComponent} />
            <Route path="/statements" component={StatementComponent} />
            
            <Route path="/applyLoan" component={NewLoanComponent} />

            <Route path="/customerConcern" component={CustomerConcernComponent} />
            <Route path="/newCustomerConcern" component={NewCustomerConcernConmponent} />
            <Route path="/myTickets" component={MyOpenTicketComponent}/>
            <Route path="/createNewTicket" component={CreateNewTicket}/>

            <Route path="**" render={
                () => (
                    <div className="text-center">
                        <article>
                            <h1 className="text-danger">No Route Configured!</h1>
                            <h4 className="text-danger">Please check your Route Configuration</h4>
                        </article>
                        <div className="mt-5">
                            <img src={img404} alt="Not Found" className="rounded" />
                        </div>
                        <h2 className="mt-5">
                            <Link exact className="nav-link" to="/">Back to Home</Link>
                        </h2>
                    </div>
                )
            } />
        </Switch>
    </Suspense>
);