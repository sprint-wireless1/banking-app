import React, { Component } from 'react';
import TextInput from '../common/TextInput';
import accountAPIClient from '../../services/account-api-client';

class StatementComponent extends Component{
    constructor(props) {
        super(props);
        this.state = {
            accountId : 0,
            startDate : "",
            endDate : "",
            transactions: [
                {
                    "transactionId": 0,
                    "transactionTimestamp": "",
                    "transactionAmount": 0,
                    "transactionType": ""
                }
            ],
            tableHide : true,
            datewiseTableHide : true,
            message : ""
        };
        this.statementHandler = this.statementHandler.bind(this);
        this.statementByDateHandler = this.statementByDateHandler.bind(this);
        this.updateState = this.updateState.bind(this);
        
    }

    statementByDateHandler(e){
        e.preventDefault();
        accountAPIClient.getStatementByDate(this.state.accountId, this.state.startDate, this.state.endDate).then(data => {
            this.setState({
                 message : "",
                 datewiseTableHide : false,
                 tableHide : true,
                 transactions: data
                });
        }).catch(eMsg => {
            this.setState({ message: eMsg, datewiseTableHide : true});    
        });
        console.log(this.state);
    }

    updateState(e) {
        if(e.target.name === "id"){
            this.setState({ accountId :  e.target.value });  
        } else if(e.target.name === "startDate") {
            this.setState({ startDate :  e.target.value });
        } else if(e.target.name === "endDate") {
            this.setState({ endDate :  e.target.value });
        }
    }

    statementHandler(e) {
        e.preventDefault();
        accountAPIClient.getStatement(this.state.accountId).then(data => {
            this.setState({
                 message : "", 
                 tableHide : false,
                 datewiseTableHide : true,
                 transactions: data.transactions
                });
        }).catch(eMsg => {
            this.setState({ message: eMsg, tableHide : true});    
        });
        console.log(this.state);
    }

    render(){
        return (
        <div className='text-center'>
            <h1 className="text-primary">BankStatement</h1>
            <form className="justify-content-center">
                <fieldset className="col-sm-6 offset-sm-3">
                    <TextInput name="id" label="Account Id" fieldType="text" value={this.state.accountId} onChange={this.updateState}/>
                    <button className="btn btn-primary" onClick={this.statementHandler}>Get Complete Statement</button>{' '}
                    <br /> <br />
                    <TableComponent tableHide={this.state.tableHide} transactions={this.state.transactions}/>
                    <br />
                    <TextInput name="startDate" label="Start Date" fieldType="date" value={this.state.startDate} onChange={this.updateState}/>
                    <TextInput name="endDate" label="End Date" fieldType="date" value={this.state.endDate} onChange={this.updateState} />
                    <p class="form-helper" style={{fontSize:"12px", textAlign:"center"}}>*Note : If date fields are empty then current date statements are displayed.</p>
                    <button className="btn btn-primary" onClick={this.statementByDateHandler}>Get Statement By Date</button>{' '}
                    <br /><br />
                    <h3 className="text-danger text-center">{this.state.message}</h3>
                    <TableComponent tableHide={this.state.datewiseTableHide} transactions={this.state.transactions}/>
                </fieldset>
            </form>
            
        
        </div>);
    }
}

const TransactionListRow = ({ transaction }) => {
  //  var [show, setShow] = useState(false);

    return (
        <>
            <tr key={transaction.transactionId}>
                <td>{transaction.transactionId}</td>
                <td>{transaction.transactionTimestamp}</td>
                <td>{transaction.transactionAmount}</td>
                <td>{transaction.transactionType}</td>
            </tr>

        </>
    );
};

export default StatementComponent;

const TableComponent = ({tableHide, transactions}) => {
    return (
        <table className="table table-secondary" hidden={tableHide}>
            <thead>
                <tr>
                    <th>Transaction Id</th>
                    <th>Time</th>
                    <th>Amount</th>
                    <th>Transaction Type</th>
                </tr>
            </thead>
            <tbody>
                {
                   transactions.map(transaction => <TransactionListRow key={transaction.transactionId} transaction={transaction} />)
                }
            </tbody>
        </table>
    );
}