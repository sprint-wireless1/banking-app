import React, { Component } from 'react';
import customerconcernAPIClient from '../../services/customerconcern-api-client';
import {Link} from 'react-router-dom'

import TextInput from '../common/TextInput';

class NewCustomerConcernConmponent extends Component{

    constructor(props) {
        super(props);
        this.state = { 
            customer:{
                            "id":0,
                            "email": "",
                            "phone": "",
                            "message": ""
                     }, readOnly: false
        }
        this.updateState = this.updateState.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
    }
  

    updateState(e) {
        const field = e.target.name;
        const value = e.target.value;
        let customer = { ...this.state.customer };
        switch(field){
            case "email":
                {
                    customer[field] = value;
                    break;
                }   
            case "phone":
                    {
                        customer[field] = value;
                        break;
                    }   
            case "message":
                {
                    customer[field] = value;
                    break;
                }   
        }
        this.setState({ customer: customer });
     }


    submitHandler(e){
  
      e.preventDefault();
        customerconcernAPIClient.riaseConcern(this.state.customer).then(data => {
            console.log(data.id);
            console.log(this.state);
            this.setState({customer: { 
                            "id":data.id,
                            "email": data.email,
                            "phone": data.phone,
                            "message": data.message
                            }, readOnly : true});
            console.log(this.state);
        }).catch(eMsg => {
            this.setState({ message: eMsg });
        });
    }

    
    render() {
        return ( 
            // <p>sdf</p>)
            <div>
            <h1 className="text-primary text-center">Create New ticket</h1>
            <div className="row">
                <div className="col-sm-6 offset-sm-3">
                  
                    <form className="justify-content-center" onSubmit={this.submitHandler} >
                        <fieldset>
                            <legend className="text-center">Enter details</legend>
                             <TextInput name="id" label="Ticket Id" fieldType="text" value={this.state.customer.id}  readOnly={true} hint="We will generate one for you"/>
                            <TextInput name="email" label="Email" fieldType="text"  value={this.state.customer.email} placeholder="Enter email" readOnly={this.state.readOnly} onChange={this.updateState} required/>
                           
                            <TextInput name="phone" label="Phone No" fieldType="number" value={this.state.customer.phone} placeholder="Enter Phone Number" readOnly={this.state.readOnly} onChange={this.updateState} />
                            <TextInput name="message" label="Message" fieldType="text" value={this.state.customer.message} placeholder="Enter your message" readOnly={this.state.readOnly} onChange={this.updateState} />
                            <div className="text-center">
                                <button type='submit' className="btn btn-primary">Create</button>{' '}
                                {/* <button type='reset' className="btn btn-secondary">Cancal</button> */}
                                <br /><br />
                                <Link to="/createNewTicket" className="nav-link d-flex flex-column align-items-center">Create New ticket</Link>
                            </div>
                        </fieldset>
                    </form>
                </div> 
            </div>
        </div>)
    }
 }


 export default NewCustomerConcernConmponent;