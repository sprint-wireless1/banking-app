import React, { Component } from 'react'

import TextInput from '../common/TextInput';
import customerconcernAPIClient from '../../services/customerconcern-api-client';

export class MyOpenTicketComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            "id":0,
            "email": "",
            "phone": "",
            "message": ""
        };
        this.ticketDetailsHandler = this.ticketDetailsHandler.bind(this);
        this.updateState = this.updateState.bind(this);
    }

    updateState(e) {
        this.setState({ id:  e.target.value });  
    }

    ticketDetailsHandler(e){
        e.preventDefault();
        customerconcernAPIClient.getTicketDetails(this.state.id).then(data => {
            this.setState({customer: {          
                            "id":data.id,
                            "email": data.email,
                            "phone": data.phone,
                            "message": data.message }, 
                            
                            showMessage : "id : " + data.id + "   email : " + data.email 
                + " phone : " + data.phone + "    message : " + data.message  });
        }).catch(eMsg => {
            this.setState({ showMessage: eMsg });
        });
    }

    render() {
        return (
        
            <div>
                <h1 className="text-primary text-center">Ticket Information</h1>
                <form className="justify-content-center">
                        <fieldset className="col-sm-4 offset-sm-4">
                            <legend className="text-center">Enter Ticekt Id</legend>
                            <TextInput name="id" label="Ticket Id" fieldType="text" value={this.state.id} onChange={this.updateState}/>
                            <div className="text-center">
                                <button className="btn btn-primary" onClick={this.ticketDetailsHandler}>Get Ticket Details</button>{' '}
                            </div>
                        </fieldset>
                </form>
            </div>
           
     
        );
    }
}

export default MyOpenTicketComponent
