import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import TextInput from '../common/TextInput';

import customerconcern from '../../services/customerconcern-api-client';



import './CustomerConcernComponent.css';

class CustomerConcernComponent extends Component {

    render() {
        return (
            <>
            <div>
                <form className="justify-content-center">
                        <fieldset className="col-sm-4 offset-sm-4">
                            <div className="text-center">
                                {/* <button className="btn btn-primary" onClick={this.raiseConcernHandler}>Raise Concern</button>{' '}
                                <button className="btn btn-primary" onClick={this.shareFeedbackHandler}>Share Feedback</button>{' '} */}
                              <Link to="/newCustomerConcern" className="nav-link d-flex flex-column align-items-center" >Raise Concern</Link>
                             
                            </div  >
                            <div className="text-center"> <Link to="/myTickets" className="nav-link d-flex flex-column align-items-center">My open ticket</Link></div>
                        </fieldset>
                </form>
            </div>
          
        </>
        );
    }
}
export default CustomerConcernComponent;