import React from 'react';

const HomeComponent = () => {
    return (
        <div>
            <h1 className="text-info">Welcome to State Bank of Mysore Web Banking!!</h1>
            <h4 className="text-warning">This is a on spot solution for your banking needs.</h4>
        </div>
    );
};

export default HomeComponent;