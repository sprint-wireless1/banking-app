/* eslint-disable */
import React, { Component } from 'react';
//import accountAPIClient from '../../services/account-api-client';
import loanAPIClient from '../../services/loan-api-client';

import TextInput from '../common/TextInput';

class NewLoanComponent extends Component{

    constructor(props) {
        super(props);
        this.state = {loan : {
        loan_amount: "",
        balance_tenure: "",
        tenure : 0,
        status: ""}, message : ""}
        this.updateState = this.updateState.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
    }

    updateState(e) {
        const field = e.target.name;
        const value = e.target.value;
        const loan = {...this.state.loan}
        switch(field){
            case "loan_amount":
                {
                    loan[loan_amount] =  value;
                    break;
                }
            case "balance_tenure":
                {
                    loan[balance_tenure] = value;
                }
            case "status" :
                {
                    loan[status] = value;
                    break;
                }
                this.setState({ loan : loan });
        }
        
    }


    submitHandler(e){
        e.preventDefault();
        loanAPIClient.insertAccount(this.state).then(data => {
            console.log(data.accountId);
            console.log(this.state);
            this.setState({loan : {
                loan_amount: data.loan_amount,
                balance_tenure: data.balance_tenure,
                tenure : data.tenure,
                status: data.status}, 
                message : "Loan Applied successfully."});        
            console.log(this.state);
        }).catch(eMsg => {
            this.setState({ message: eMsg });
        });
    }

    render() {
       return ( 
       <div>
            <h1 className="text-primary text-center">Apply For Loan</h1>
            <div className="row">
                <div className="col-sm-6 offset-sm-3">
                    <form className="justify-content-center" onSubmit={this.submitHandler}>
                        <fieldset>
                            <legend className="text-center">Enter Bellow Information</legend>
                            {/* <TextInput name="id" label="Account Id" fieldType="text" value={this.state.loan.accountId} readOnly={true} hint="We will generate one for you"/> */}
                            <TextInput name="loan_amount" label="Loan Amount" fieldType="number" value={this.state.loan_amount} onChange={this.updateState}/>
                            <TextInput name="balance_tenure" label="Loan Tenure" fieldType="number" value={this.state.balance_tenure} onChange={this.updateState}  />
                            <TextInput name="status" label="Status" fieldType="text" value={this.state.status} onChange={this.updateState} />
                        
                            {/* <TextInput name="accountBalance" label="Account Balance" fieldType="number" value={this.state.account.accountBalance} onChange={this.updateState} readOnly={this.state.readOnly}/> */}
                            {/* <TextInput name="accountBalance" label="Account Balance" fieldType="number" value={this.state.loan.accountBalance} readOnly={true} hint="We will get from  your balance from account id"/> */}
                            <div className="text-center">
                                <button type='submit' className="btn btn-primary">Save</button>{' '}
                                <button type='reset' className="btn btn-secondary">Reset</button>
                            </div>
                            <h3>{this.state.message}</h3>
                        </fieldset>
                    </form>
                </div> 
            </div>
        </div>)
   }
}

export default NewLoanComponent;