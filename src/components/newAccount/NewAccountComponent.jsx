/* eslint-disable */
import React, { Component } from 'react';
import accountAPIClient from '../../services/account-api-client';

import TextInput from '../common/TextInput';

class NewAccountComponent extends Component{

    constructor(props) {
        super(props);
        this.state = { 
            account: {          
                "accountId": 0,
                "customer": {
                            "customerId": 0,
                            "name": "",
                            "email": "",
                            "phoneNumber": "",
                            "address": ""
                            },
                "accountBalance": "" }, readOnly: false, message : ""
        }
        this.updateState = this.updateState.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
        this.resetHandler = this.resetHandler.bind(this);
    }

    resetHandler(e){
        this.setState({account: {          
            "accountId": 0,
            "customer": {
                        "customerId": 0,
                        "name": "",
                        "email": "",
                        "phoneNumber": "",
                        "address": ""
                        },
            "accountBalance": "" }, readOnly: false, message : ""});
    }

    updateState(e) {
        const field = e.target.name;
        const value = e.target.value;
        let account = { ...this.state.account };
        switch(field){
            case "accountBalance":
                {
                    account[field] = value;
                    break;
                }
            default :
                {
                    account.customer[field] = value;
                    break;
                }
        }
        this.setState({ account: account });
    }

    submitHandler(e){
        e.preventDefault();
        accountAPIClient.insertAccount(this.state.account).then(data => {
            this.setState({account: {          
                "accountId": data.accountId,
                "customer": {
                            "customerId": data.customer.customerId,
                            "name": data.customer.name,
                            "email": data.customer.email,
                            "phoneNumber": data.customer.phoneNumber,
                            "address": data.customer.address
                            },
                "accountBalance": data.accountBalance }, readOnly : true, message : "Account successfully opened with account id : " + data.accountId});
        }).catch(eMsg => {
            this.setState({ message: eMsg });
        });
    }

    render() {
       return ( 
       <div>
            <h1 className="text-primary text-center">Open New Account</h1>
            <div className="row">
                <div className="col-sm-6 offset-sm-3">
                    <form className="justify-content-center" onSubmit={this.submitHandler} onReset={this.resetHandler}>
                        <fieldset>
                            <legend className="text-center">Enter User Information</legend>
                            <TextInput name="id" label="Account Id" fieldType="text" value={this.state.account.accountId} readOnly={true} hint="We will generate one for you"/>
                            <TextInput name="name" label="Name" fieldType="text" value={this.state.account.customer.name} onChange={this.updateState} readOnly={this.state.readOnly} hint="Full Name"/>
                            <TextInput name="email" label="Email" fieldType="text" value={this.state.account.customer.email} onChange={this.updateState} readOnly={this.state.readOnly} />
                            <TextInput name="address" label="Address" fieldType="text" value={this.state.account.customer.address} onChange={this.updateState} readOnly={this.state.readOnly}/>
                            <TextInput name="phoneNumber" label="Phone No" fieldType="number" value={this.state.account.customer.phoneNumber} onChange={this.updateState} readOnly={this.state.readOnly} hint="Should be a 10 digit mobile no"/>
                            <TextInput name="accountBalance" label="Account Balance" fieldType="number" value={this.state.account.accountBalance} onChange={this.updateState} readOnly={this.state.readOnly} hint="Should be a positive amount"/>
                            <div className="text-center">
                                <button type='submit' className="btn btn-primary">Save</button>{' '}
                                <button type='reset' className="btn btn-secondary">Reset</button>
                            </div>
                            <h3 className='text-danger text-center'>{this.state.message}</h3>
                        </fieldset>
                    </form>
                </div> 
            </div>
        </div>)
   }
}

export default NewAccountComponent;