import React from 'react';
import { NavLink } from 'react-router-dom';

import SwitchComponent from '../../routes';

import './NavigationComponent.css';

var logo = require('../../logo.svg').default;

const NavigationComponent = () => {
    return (
        <>
            <nav className="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
                <div className="container-fluid">
                    <NavLink className="navbar-brand d-flex flex-column align-items-center" to="/">
                        {/* <img src={logo} alt="React" width="40" height="28" className="d-inline-block align-text-top" /> */}
                        State Bank of Mysore
                    </NavLink>

                    <button type="button" className="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#myNavbar">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="myNavbar">
                        <ul className="navbar-nav ms-auto">
                            <li className="nav-item px-3">
                                <NavLink exact className="nav-link d-flex flex-column align-items-center" to="/">
                                    <i className="bi bi-house-fill"></i>
                                    <span>Home</span>
                                </NavLink>
                            </li>

                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/newAccount">
                                    <i className="bi bi-person-plus-fill"></i>
                                    <span>Open New Account</span>
                                </NavLink>
                            </li>

                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/account">
                                    <i className="bi bi-inbox"></i>
                                    <span>Account</span>
                                </NavLink>
                            </li>

                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/transaction">
                                    <i className="bi bi-cash-coin"></i>
                                    <span>Transaction</span>
                                </NavLink>
                            </li>

                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/statements">
                                    <i className="bi bi-file-text"></i>
                                    <span>Statements</span>
                                </NavLink>
                            </li>

                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/applyLoan">
                                    <i className="bi bi-123"></i>
                                    <span>Apply Loan</span>
                                </NavLink>
                            </li>
                            
                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/customerConcern">
                                    <i className="bi bi-person-fill"></i>
                                    <span>Customer Service</span>
                                </NavLink>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </nav>

            <>
                {SwitchComponent}
            </>
        </>
    );
};

export default NavigationComponent;