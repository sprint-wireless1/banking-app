import React from 'react';
import PropTypes from 'prop-types';

const TextInput = ({ name, label, fieldType, placeholder, value, readOnly, onChange, hint }) => {
    return (
        <div className="form-group mb-1">
            <label className="mb-0" htmlFor={name} >{label}</label>
            <input type={fieldType} className="form-control" id={name} name={name}
                placeholder={placeholder} value={value}
                onChange={onChange} readOnly={readOnly} />
            <div class="form-helper" style={{fontSize:"12px", textAlign:"right"}}>{hint}</div>
        </div>
    );
};

TextInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    fieldType: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    readOnly: PropTypes.bool,
    onChange: PropTypes.func,
    hint: PropTypes.string
};

export default TextInput;