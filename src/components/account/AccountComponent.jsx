import React, { Component } from 'react';
import TextInput from '../common/TextInput';

import accountAPIClient from '../../services/account-api-client';

import './AccountComponent.css';

class AccountComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accountId : 0,
            accountBal : 0,
            account: {          
                "accountId": 0,
                "customer": {
                            "customerId": 0,
                            "name": "",
                            "email": "",
                            "phoneNumber": "",
                            "address": ""
                            },
                "accountBalance": 0 },
            message : ""
        };
        this.accountDetailsHandler = this.accountDetailsHandler.bind(this);
        this.accountBalanceHandler = this.accountBalanceHandler.bind(this);
        this.updateState = this.updateState.bind(this);
        
    }
    
    updateState(e) {
        this.setState({ accountId:  e.target.value });  
    }
    
    accountBalanceHandler(e){
        e.preventDefault();
        accountAPIClient.getAccountBalance(this.state.accountId).then(data => {
            this.setState({ accountBal : data, message : "Account Balance : ₹" + data });
        }).catch(eMsg => {
            this.setState({ message: eMsg});
        });
    }

    accountDetailsHandler(e){
        e.preventDefault();
        accountAPIClient.getAccount(this.state.accountId).then(data => {
            this.setState({account: {          
                "accountId": data.accountId,
                "customer": {
                            "customerId": data.customer.customerId,
                            "name": data.customer.name,
                            "email": data.customer.email,
                            "phoneNumber": data.customer.phoneNumber,
                            "address": data.customer.address
                            },
                "accountBalance": data.accountBalance }, message : "Name : " + data.customer.name + "   Email : " + data.customer.email 
                + " Phone No : " + data.customer.phoneNumber + "    Address : " + data.customer.address + " Account Balance : ₹" + data.accountBalance });
        }).catch(eMsg => {
            this.setState({ message: eMsg });
        });
    }

    render() {
        return (
            <>
            <div>
                <h1 className="text-primary text-center">Account Information</h1>
                <form className="justify-content-center">
                        <fieldset className="col-sm-4 offset-sm-4">
                            <legend className="text-center">Enter Account Id</legend>
                            <TextInput name="id" label="Account Id" fieldType="number" value={this.state.accountId} onChange={this.updateState}/>
                            <div className="text-center">
                                <button className="btn btn-primary" onClick={this.accountBalanceHandler}>Get Balance</button>{' '}
                                <button className="btn btn-primary" onClick={this.accountDetailsHandler}>Get Account Details</button>{' '}
                            </div>
                        </fieldset>
                </form>
            </div>
            <div className="col">
                <br />
                <hr />
                <AccountQueryResponse  message={this.state.message}/>
            </div>
        </>
        );
    }
}

export default AccountComponent;

const AccountQueryResponse = ({message}) => {
    return (
        <>
        <h3 className="text-primary text-center">{message}</h3>
        </>
    );
}