import React, { Component } from 'react';
import TextInput from '../common/TextInput';

import accountAPIClient from '../../services/account-api-client';

class AccountComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accountId : 0,
            transaction :{
                "transactionAmount" : 0,
                "transactionType" : ""
            },
            message : ""
        };
        this.transactionHandler = this.transactionHandler.bind(this);
        this.updateState = this.updateState.bind(this);
    }

    updateState(e) {
        console.log(e.target.name + ' : ' + e.target.value)
        if(e.target.name == "id"){
            this.setState({ accountId:  e.target.value });  
        } else{
            let transaction = { ...this.state.transaction };
            transaction[e.target.name] = e.target.value;
            this.setState({ transaction:  transaction });
        }
    }
    
    transactionHandler(e){
        e.preventDefault();
        accountAPIClient.transact(this.state.accountId, this.state.transaction).then(data => {
            if(data.transactionType == "CREDIT"){
                this.setState({ transaction : data, 
                    message : "Account credited with amount : ₹" + data.transactionAmount, 
                    transactionType : "" });
            } else {
                this.setState({ transaction : data, 
                    message : "Account debited with amount : ₹" + data.transactionAmount, 
                    transactionType : "" });
            }
            
        }).catch(eMsg => {
            this.setState({ message: eMsg, 
                transactionType : ""});
        });
    }

    render() {
        return (
            <>
            <div>
                <h1 className="text-primary text-center">Transaction</h1>
                <form className="justify-content-center">
                        <fieldset className="col-sm-4 offset-sm-4">
                            <legend className="text-center">Enter Account Id</legend>
                            <TextInput name="id" label="Account Id" fieldType="number" value={this.state.accountId} onChange={this.updateState}/>
                            <TextInput name="transactionAmount" label="Transaction Amount" fieldType="number" value={this.state.transaction.transactionAmount} onChange={this.updateState}/>                
                            <div onChange={this.updateState} className='text-center'>
                            <input type="radio" value="CREDIT" name="transactionType" /> CREDIT{' '}
                            <input type="radio" value="DEBIT" name="transactionType" /> DEBIT
                            <br /><br />
                            </div>
                            <div className="text-center">
                                <button className="btn btn-primary" onClick={this.transactionHandler}>Transact</button>
                            </div>
                        </fieldset>
                </form>
            </div>
            <div className="col">
                <br />
                <hr />
                <AccountQueryResponse  message={this.state.message}/>
            </div>
        </>
        );
    }
}

export default AccountComponent;

const AccountQueryResponse = ({message}) => {
    return (
        <>
        <h3 className="text-primary text-center">{message}</h3>
        </>
    );
}